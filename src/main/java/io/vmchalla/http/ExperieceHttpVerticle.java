package io.vmchalla.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * This Http Verticle exposes rest APIs for Experience Module
 * REST methods Supported : POST, GET, PUT, DELETE
 *
 */
public class ExperieceHttpVerticle extends AbstractVerticle{

  private static final Logger LOGGER = LoggerFactory.getLogger(ExperieceHttpVerticle.class);
  public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";
  public static final String CONFIG_MONGO_DB_QUEUE = "mongo.queue";

  //Used for Event Bus Message Exchanges Between DB Module and Http Module
  private String mongoQueue = "mongo.queue";

  @Override
  public void start(Future<Void> httpFuture) throws Exception{
    //Configure Event Bus message queue between the modules
    mongoQueue = config().getString(CONFIG_MONGO_DB_QUEUE, "mongo.queue");
    HttpServer httpServer = vertx.createHttpServer(); //Create Server

    //Router Configurations for Experience Module
    Router router = Router.router(vertx);
    router.get("/experience").handler(this::getTotalExperience);
    router.get("/currentwork/experience").handler(this::getMostRecentExperience);
    router.get("/experience/location/:location").handler(this::getExperienceInSpecificLocation);
    router.get("/experience/client/:client").handler(this::getExperienceAtSpecificClient);
    //BodyHandler For POST and PUT
    router.route("/experience*").handler(BodyHandler.create());
    router.post("/experience/add").handler(this::addNewExpererience);
    router.put("/experience/update").handler(this::updateExperience);
    router.delete("/experience/:id").handler(this::removeErroneousExperiece);//Should Know the ID to delete it
  }

  /**
   * get details of total experience gathered till date
   * @param context
   */
  public void getTotalExperience(RoutingContext context){

  }

  /**
   * get most recent Experience
   * @clue endDate is Current
   * @param context
   */
  public void getMostRecentExperience(RoutingContext context){

  }

  /**
   * Get Experience gathered at a specific location
   * @param context
   */
  public void getExperienceInSpecificLocation(RoutingContext context){

  }

  /**
   * Get Experience gathered while working with Specific Client
   * @param context
   */
  public void getExperienceAtSpecificClient(RoutingContext context){

  }

  /**
   * Update the experience if there has been an error
   * @param context
   */
  public void updateExperience(RoutingContext context){

  }

  /**
   * Add New Experience Gathered while Still working :)
   * @param context
   */
  public void addNewExpererience(RoutingContext context){

  }

  /**
   * Remove a bad experience (after All Life is short; Why Document Bad Experiences)
   * @param context
   */
  public void removeErroneousExperiece(RoutingContext context){

  }

}
