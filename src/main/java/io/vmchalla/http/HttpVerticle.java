package io.vmchalla.http;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServer;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class HttpVerticle extends AbstractVerticle{

  private static final Logger LOG = LoggerFactory.getLogger(HttpVerticle.class);
  public static final String CONFIG_HTTP_SERVER_PORT = "http.server.port";

  @Override
  public void start(Future<Void> httpFuture) throws Exception{
    LOG.info("Starting HTTP Verticle");
    //Create Server
    HttpServer httpServer = vertx.createHttpServer();
    int portNumber = config().getInteger(CONFIG_HTTP_SERVER_PORT, 8080);

    //Router Configuration
    Router router = Router.router(vertx);
    router.route().handler(BodyHandler.create());
    //1. Experience
    router.get("/experience").handler(this::getTotalExperience);
    router.get("/experience/currentwork").handler(this::getMostRecentExperience);
    router.get("/experience/location/:location").handler(this::getExperienceInSpecificLocation);
    router.get("/experience/client/:client").handler(this::getExperienceAtSpecificClient);
    router.post("/experience").handler(this::addNewExpererience);
    router.put("/experience").handler(this::updateExperience);
    router.delete("/experience/:id").handler(this::removeErroneousExperiece);
    //2. Education
    router.get("/education").handler(this::getFullEducationDetails);
    router.get("/education/degree/:degree").handler(this::getDegreeByName);
    router.post("/education").handler(this::addNewDegree);
    router.put("/education").handler(this::updateDegree);
    router.delete("education/:id").handler(this::removeErroneousDegree);
    //3. Projects
    router.get("/projects").handler(this::getAllProjects);
    router.get("/projects/client/:client").handler(this::getProjectsByClient);
    router.get("/projects/academic").handler(this::getAcademicProjects);
    router.put("/projects").handler(this::updateProject);
    router.delete("/projects/:id").handler(this::removeProject);
    router.post("/projects").handler(this::addNewProject);
    //4. Testimonials
    router.get("/testimonials").handler(this::getTestimonials);
    router.get("testimonials/:recommender").handler(this::getTestimonialByRecommederName);
    router.post("/testimonials").handler(this::addTestimonial);
    router.put("/testimonials").handler(this::updateTestimonial);
    router.delete("/testimonials/:id").handler(this::removeErroneousTestimonial);
    //5. SkillSet
    router.get("/skillset").handler(this::getSkillSet);
    router.get("/skillset/:domain").handler(this::getSkillByDomain);
    router.post("/skillset").handler(this::addNewSkill);
    router.post("/skillset/domain").handler(this::addNewSkillDomain);
    router.put("/skillset").handler(this::updateSillSet);
    router.delete("/skillset/:id").handler(this::removeUnlearnedSkill);
  }

  //------------------------------------Experience--------------------------------------------------------
  private void getTotalExperience(RoutingContext context){

  }

  private void getMostRecentExperience(RoutingContext context){

  }

  private void getExperienceInSpecificLocation(RoutingContext context){

  }

  private void getExperienceAtSpecificClient(RoutingContext context){

  }

  private void updateExperience(RoutingContext context){

  }

  private void addNewExpererience(RoutingContext context){

  }

  private void removeErroneousExperiece(RoutingContext context){

  }

  //--------------------------------------Education------------------------------------------------------------
  private void getFullEducationDetails(RoutingContext context){

  }

  private void getDegreeByName(RoutingContext context){

  }

  private void addNewDegree(RoutingContext context){

  }

  private void removeErroneousDegree(RoutingContext context){

  }

  private void updateDegree(RoutingContext context){

  }

  //-------------------------------------------Projects---------------------------------------------------------
  private void getAllProjects(RoutingContext context){

  }

  private void getProjectsByClient(RoutingContext context){

  }

  private void getAcademicProjects(RoutingContext context){

  }

  private void updateProject(RoutingContext context){

  }

  private void removeProject(RoutingContext context){

  }

  private void addNewProject(RoutingContext context){

  }

  //----------------------------------------Testimonials----------------------------------------------------------
  private void getTestimonials(RoutingContext context){

  }

  private void getTestimonialByRecommederName(RoutingContext context){

  }

  private void updateTestimonial(RoutingContext context){

  }

  private void removeErroneousTestimonial(RoutingContext context){

  }

  private void addTestimonial(RoutingContext context){

  }

  //-----------------------------------------SkillSet--------------------------------------------------------------
  private void getSkillSet(RoutingContext context){

  }

  private void getSkillByDomain(RoutingContext context){

  }

  private void addNewSkill(RoutingContext context){

  }

  private void addNewSkillDomain(RoutingContext context){

  }

  private void updateSillSet(RoutingContext context){

  }

  private void removeUnlearnedSkill(RoutingContext context){

  }
}
