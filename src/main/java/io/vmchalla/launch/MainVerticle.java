package io.vmchalla.launch;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vmchalla.modal.Education;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainVerticle extends AbstractVerticle {

  @Override
  public void start(Future<Void> startFuture) {
    //For startes:
    createEducationData();
    //Routes : These objects let you define how requests are handled
    Router router = Router.router(vertx);
  //Static Page
    router.route("/assets/*").handler(StaticHandler.create("assets"));

//-------------------------------------------------------------------------------------------------
    // Educational Detils Rest API:
    //getAll
    router.get("/api/education").handler(apiRoutingContext -> {
      apiRoutingContext.response().putHeader("content-type", "application/json; charset=utf-8")
        .end(Json.encodePrettily(educationDetails.values()));
    });
    //AddOne
    router.route("/api/education*").handler(BodyHandler.create());
    router.post("/api/education").handler( postRoutingContext -> {
      Education newInstitute = Json.decodeValue(postRoutingContext.getBodyAsString(), Education.class);
      educationDetails.put(newInstitute.getId(), newInstitute);
      postRoutingContext.response().putHeader("content-type", "application/json; charset=utf-8")
        .setStatusCode(201).end(Json.encodePrettily(newInstitute));
    });
    //DeleteOne
    router.delete("/api/education/:id").handler(deleteRoutingContext -> {
      String id = deleteRoutingContext.request().getParam("id");
      if(id==null){
        deleteRoutingContext.response().setStatusCode(400).end();
      } else {
        Integer idAsInteger = Integer.valueOf(id);
        educationDetails.remove(idAsInteger);
        deleteRoutingContext.response().setStatusCode(204).end();
      }
    });
    //updateOne
    router.put("/api/education/:id").handler(patchRoutingContext -> {
      String id = patchRoutingContext.request().getParam("id");
      if(id==null){
        patchRoutingContext.response().setStatusCode(400).end();
      } else {
        Integer idAsInteger = Integer.valueOf(id);
        Education updatesThisOne = educationDetails.get(idAsInteger);
        Education updateed = Json.decodeValue(patchRoutingContext.getBodyAsString(),Education.class);
        updatesThisOne.setGpa(updateed.getGpa());
        updatesThisOne.setInstitution(updateed.getInstitution());
        updatesThisOne.setLevel(updateed.getLevel());
        updatesThisOne.setProjects(updateed.getProjects());
        patchRoutingContext.response().putHeader("content-type", "application/json; charset=utf-8")
          .setStatusCode(200).end(Json.encodePrettily(updatesThisOne));
      }
    });
    //getOne
    router.get("/api/education/:id").handler(getOneRoutingContext -> {
      String id = getOneRoutingContext.request().getParam("id");
      if(id==null){
        getOneRoutingContext.response().setStatusCode(400).end();
      } else {
        Integer idAsInteger = Integer.valueOf(id);
        getOneRoutingContext.response().putHeader("content-type", "application/json; charset=utf-8")
          .setStatusCode(200).end(Json.encodePrettily(educationDetails.get(idAsInteger)));
      }
    });
//--------------------------------End Rest API for Education-------------------------------------------------

    //Handlers: Actual Actions processing the request and writing results : Handlers can be chained
    router.route("/").handler(routingContext -> {
      HttpServerResponse response = routingContext.response();
      response.putHeader("content-type", "text/html")
        .end("<h1>Hi there this is Vamsi Mohan Challa Building his resume</h1>");
    });

    //Create HttpServer  and pass accept method to request handler
    vertx.createHttpServer().requestHandler(router::accept)
      .listen(config().getInteger("http.port", 8090), result -> {
        if(result.succeeded()){
          startFuture.complete();
        } else {
          startFuture.fail(result.cause());
        }
      });

    //Some Rest APIs for getting started


  }

  //Education Details For getting started
  private Map<Integer, Education> educationDetails = new LinkedHashMap<>();

  //Creating the hashmap for education : (getting started)
  private void createEducationData(){
    List<String> projectList = new ArrayList<>();
    projectList.add("\"Industrial WebApplication\"");
    Education masters = new Education("Masters","Bradley University", "3.85", projectList);
    Education bachelors = new Education("Bachelors","Mannan", "3.85", projectList);
    educationDetails.put(masters.getId(), masters);
    educationDetails.put(bachelors.getId(), bachelors);
  }

}
