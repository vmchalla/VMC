package io.vmchalla.modal;

import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;

public class Education {

  //Adding a static Atomic Integer for getting started with rest API now
  private static final AtomicInteger COUNTER = new AtomicInteger();
  private Integer id;
  private String level;
  private String institution;
  private String gpa;
  private List<String> projects;
  private String degree;
  private Duration duration;
  private Location location;

  public Education(String level, String institution, String gpa, List<String> projects) {
    this.id = COUNTER.getAndIncrement();
    this.level = level;
    this.institution = institution;
    this.gpa = gpa;
    this.projects = projects;
  }

  public Education(){
    this.id = COUNTER.getAndIncrement();
  }

  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getLevel() {
    return level;
  }

  public void setLevel(String level) {
    this.level = level;
  }

  public String getInstitution() {
    return institution;
  }

  public void setInstitution(String institution) {
    this.institution = institution;
  }

  public String getGpa() {
    return gpa;
  }

  public void setGpa(String gpa) {
    this.gpa = gpa;
  }

  public List<String> getProjects() {
    return projects;
  }

  public void setProjects(List<String> projects) {
    this.projects = projects;
  }

  public String getDegree() {
    return degree;
  }

  public void setDegree(String degree) {
    this.degree = degree;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }
}
