package io.vmchalla.modal;

public class Email {

  private String id;
  private String from;
  private String subject;
  private String Body;
  private Object attachment;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getFrom() {
    return from;
  }

  public void setFrom(String from) {
    this.from = from;
  }

  public String getSubject() {
    return subject;
  }

  public void setSubject(String subject) {
    this.subject = subject;
  }

  public String getBody() {
    return Body;
  }

  public void setBody(String body) {
    Body = body;
  }

  public Object getAttachment() {
    return attachment;
  }

  public void setAttachment(Object attachment) {
    this.attachment = attachment;
  }
}
