package io.vmchalla.modal;

import java.io.Serializable;
import java.util.List;

public class Experience implements Serializable{

  private String id;
  private String client;
  private String jobTitle;
  private List<String> jobResponsibilities;
  private List<String> projects;
  private Duration duration;
  private Location location;
  private List<String> developmentEnvironment;

  public List<String> getDevelopmentEnvironment() {
    return developmentEnvironment;
  }

  public void setDevelopmentEnvironment(List<String> developmentEnvironment) {
    this.developmentEnvironment = developmentEnvironment;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClient() {
    return client;
  }

  public void setClient(String client) {
    this.client = client;
  }

  public String getJobTitle() {
    return jobTitle;
  }

  public void setJobTitle(String jobTitle) {
    this.jobTitle = jobTitle;
  }

  public List<String> getJobResponsibilities() {
    return jobResponsibilities;
  }

  public void setJobResponsibilities(List<String> jobResponsibilities) {
    this.jobResponsibilities = jobResponsibilities;
  }

  public List<String> getProjects() {
    return projects;
  }

  public void setProjects(List<String> projects) {
    this.projects = projects;
  }

  public Duration getDuration() {
    return duration;
  }

  public void setDuration(Duration duration) {
    this.duration = duration;
  }



  public Location getLocation() {
    return location;
  }

  public void setLocation(Location location) {
    this.location = location;
  }
}
