package io.vmchalla.modal;

public class Testimonial {

  private String id;
  private String by;
  private String testimonial;
  private String contact;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getBy() {
    return by;
  }

  public void setBy(String by) {
    this.by = by;
  }

  public String getTestimonial() {
    return testimonial;
  }

  public void setTestimonial(String testimonial) {
    this.testimonial = testimonial;
  }

  public String getContact() {
    return contact;
  }

  public void setContact(String contact) {
    this.contact = contact;
  }
}
